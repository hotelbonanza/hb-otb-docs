HOTEL BONANZA - OTA Specification v1.0beta
==========================================

Whilst in beta this is a working document.

The HotelBonanza OTA API allows third parties to connect to manage rates and availability and recieve reservation requests.

The API is follows OpenTravel OTA specification. We serialise to the lastest 2016B-1.0 Version of the OpenTravel Schema 

STAGING API: [https://ota-staging.hotelbonanza.com](https://ota-staging.hotelbonanza.com)
PROD API: [https://ota.hotelbonanza.com](https://ota.hotelbonanza.com)

To request access please email api@hotelbonanza.com for Credentials and Test Hotel.

## Authentication

Each request must contain a POS field and be signed with your partner key (MessagePassword) and RequestorID ID.
You will be given full credentials

    <POS>
        <Source>
          <RequestorID MessagePassword="urI8e6g8LaR9levoL0" Type="1" ID="HBZ"/>
        </Source>
    </POS>

If you are using soap please user your Username and MessagePassword within soap security headers & PoS node. Ontop of the indiviual endpoints below we have combined OTA_HotelAvail, OTA_HotelAvailNotif & OTA_HotelAvailNotif in a sinle endpoint for soap routing. Please ensure SOAPAction header is set for the soap message to route correctly.

STAGING API: [https://ota-staging.hotelbonanza.com](https://ota-staging.hotelbonanza.com/v1/Inventory)
PROD API: [https://ota.hotelbonanza.com](https://ota-staging.hotelbonanza.com/v1/Inventory)

API MANAGEMENT: We use Apigee to help route messages, protect against treats, increase security and control api access. 

DATABASE: We run a continous development environment. The staging database is currently a copy of live and is reset on a daily basis.

PERFORMANCE: Messages are accepted and then processed via a backend system. Some Warnings & Errors may not be initially sent in response.   


## OTA_HotelAvail

Used by partner to retrieve a list of available rooms and rates.

| Request Type | HTTP POST                                              |
|:-------------|:-------------------------------------------------------|
| URL          | https://ota-staging.hotelbonanza.com/v1/OTA_HotelAvail |
|              | https://ota.hotelbonanza.com/v1/OTA_HotelAvail         |
| Request      | OTA_HotelAvailRQ                                       |
| Response     | OTA_HotelAvailRS                                       |

**EXAMPLE REQUEST:**

<OTA_HotelAvailRQ xmlns="http://www.opentravel.org/OTA/2003/05" Version="1.0" TimeStamp="2016-09-21T08:18:40+00:00" EchoToken="6cce2cd0-25e1-11e7-9598-0800200c9a66" AvailRatesOnly="true">
<POS>
<Source>
<RequestorID MessagePassword="XXXXXX" Type="1" ID="XXXXXXX"/>
</Source>
</POS> 
<AvailRequestSegments>
<AvailRequestSegment AvailReqType="Room">
<HotelSearchCriteria>
<Criterion>
<HotelRef HotelCode="XXXXXXX"/>
</Criterion>
</HotelSearchCriteria>
</AvailRequestSegment>
</AvailRequestSegments>
</OTA_HotelAvailRQ>

**EXAMPLE RESPONSE:**

<?xml version="1" encoding="UTF-8"?>
<OTA_HotelAvailRS xmlns="http://www.opentravel.org/OTA/2003/05" TimeStamp="2016-09-21T08:18:40+00:00" EchoToken="6cce2cd0-25e1-11e7-9598-0800200c9a66">
<Success/>
<RoomStays>
<RoomStay>
<RoomTypes>
<RoomType RoomTypeCode="123">
<RoomDescription Name="Single Room"/>
</RoomType>
</RoomTypes>
<RatePlans>
<RatePlan RatePlanCode="987">
<RatePlanDescription Name="Single Room Non-refundable and Self-catering"/>
</RatePlan>
</RatePlans>
</RoomStay>
<RoomStay>
<RoomTypes>
<RoomType RoomTypeCode="123">
<RoomDescription Name="Single Room"/>
</RoomType>
</RoomTypes>
<RatePlans>
<RatePlan RatePlanCode="786">
<RatePlanDescription Name="Single Room Standard cancellation and Self-catering"/>
</RatePlan>
</RatePlans>
</RoomStay>
<RoomStay>
<RoomTypes>
<RoomType RoomTypeCode="456">
<RoomDescription Name="Basic Double Room with View"/>
</RoomType>
</RoomTypes>
<RatePlans>
<RatePlan RatePlanCode="111">
<RatePlanDescription Name="Basic Double Room with View - Standard cancellation and Self-catering"/>
</RatePlan>
</RatePlans>
</RoomStay>
</RoomStays>
</OTA_HotelAvailRS>

- EchoToken - the UUID echo token should match the one sent
- AvailReqType - always set to "Room"



## OTA_HotelAvailNotif

Used by partner to update the number of rooms available on a particular date.

Used to add restrictions for a particular rate/room/date - TBC IN PROGRESS.

The method should be called each time the above information is updated.

| Request Type | HTTP POST                                                    |
|:-------------|:-------------------------------------------------------------|
| URL          | https://ota-staging.hotelbonanza.com/v1/OTA_HotelAvailNotif  |
|              | https://ota.hotelbonanza.com/v1/OTA_HotelAvailNotif          |
| Request      | OTA_HotelAvailNotifRQ                                        |
| Response     | OTA_HotelAvailNotifRS                                        |

**EXAMPLE REQUEST:**

    <OTA_HotelAvailNotifRQ xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.opentravel.org/OTA/2003/05" xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05 OTA_HotelAvailNotifRQ.xsd" TimeStamp="2016-07-29T15:35:10" Target="Production" Version="1.00"> 
        <AvailStatusMessages> 
            <AvailStatusMessage BookingLimit="25" LocatorID="1"> 
                <StatusApplicationControl Start="2016-08-17" End="2016-08-21" InvTypeCode="XXXXXXXXX"/> 
            </AvailStatusMessage> 
            <AvailStatusMessage LocatorID="2"> 
                <StatusApplicationControl Start="2016-08-17" End="2016-08-21" InvTypeCode=" XXXXXXXXX " RatePlanCode="YYYYYY" /> 
                    <LengthsOfStay> 
                        <LengthOfStay Time="4" MinMaxMessageType="SetMinLOS"/> 
                        <LengthOfStay Time="7" MinMaxMessageType="SetMaxLOS"/> 
                    </LengthsOfStay> 
                <RestrictionStatus Status="Open"/> 
            </AvailStatusMessage> 
        </AvailStatusMessages> 
    </OTA_HotelAvailNotifRQ>

**EXAMPLE RESPONSE:**

    <?xml version="1.0" encoding="UTF-8"?>
    <OTA_HotelAvailNotifRS xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05 OTA_HotelAvailNotifRS.xsd" TimeStamp="2016-07-29T15:35:12" Target="Production" Version="1.00">
        <Success />
    </OTA_HotelAvailNotifRS>

The above example will do the following:

- Set rooms to sell for room XXXXXXXXX to 25 from the 17th of August to
  and including the 21st of August 2016
- Set Minimum Stay Through for room XXXXXXXXX and rate YYYYYY to 4
  nights from the 17th of August to and including the 21st of August
  2016
- Set Maximum Stay Through for room XXXXXXXXX and rate YYYYYY to 7
  nights from the 17th of August to and including the 21st of August
  2016


## OTA_HotelAvailNotifRQ

All the below XML is fixed and mandatory, except: timestamp: Should
contain the current time and date target: Either &#39;Production&#39;
or, may contain &#39;Test&#39;, which will not update anything in the
HotelBonanza.com database.


#### AvailStatusMessages

The AvailStatusMessages element is mandatory for the OTA_HotelAvailNotif request.

    <AvailStatusMessages>
     ...
    </AvailStatusMessages>


#### BookingLimit

Bookinglimit should be left out when updating restrictions in the same "AvailStatusMessage" block, because a "RatePlanCode" attribute is mandatory in that case and BookingLimit is updated on room level, not rate level.

Availability can be updated until 254 rooms to sell. 255 will set the room to freesale (*) , which means that there is no limit of amount of rooms to sell than can be sold until the room/rate/date combination is closed with a restriction or until the rooms to sell are decreased again. Setting the value of BookingLimit to 256 or higher, will result in 254 rooms to sell in the HotelBonanza.com system.

LocatorID (required): should contain a unique ID (used as RecordID in OTA_HotelAvailNotifRS)

    <AvailStatusMessage BookingLimit="25" LocatorID="1">

#### StatusApplicationControl

Start / End (required): the period which you are updating (inclusive end date). 
Please note, only dates in the future can be updated. 

- RatePlanCode: the HotelBonanza.com rate ID which you are updating.
    *Needs to be left out when specifying Bookinglimit in AvailStatusMessage, as the availability is updated on room level.*
- InvTypeCode (required) the HotelBonanza.com room ID which you are updating.

.

    <StatusApplicationControl Start="2016-11-05" End="2016-11-17" InvTypeCode="36745603" RatePlanCode="1278606" />

### Optional elements

#### LengthsOfStay

Is mandatory when specifying element LengthOfStay.

ArrivalDateBased can be set to 0 or 1. If this boolean value is set to 1, the restriction set has an effect only on the arrival day of a booking, whereas the '0' value may affect a search for availability or reservation on all the dates that the query covers. The ArrivalDateBased attribute is optional and when left out, '0' is assumed.

    <LengthsOfStay ArrivalDateBased="0"> 
        <LengthOfStay ... /> 
    </LengthsOfStay>

####LengthOfStay

The minimum or maximum stay for the given room for the given date for the given rate category. If a booking takes place on this day a minimum or maximum stay (for the whole booking or arrival based, depending on ArrivalDateBased="0" or ArrivalDateBased="1") of this amount of days is required. Each day in a stay has a room and rate category ID associated with it. Each set of consecutive days with the same rate category ID in a stay, must comply with the minimum or maximum stay setting.

    <LengthOfStay Time="2" MinMaxMessageType="SetMinLOS"/> 
    <LengthOfStay Time="5" MinMaxMessageType="SetMaxLOS"/>

####RestrictionStatus

###### Mandatory attributes:

**Status=""**

If set to "Close" (or "Open"), this room will be closed (or opened) for the given date for the given rate category. All other information (Bookinglimit, prices, etc) is preserved. The 'Status' attribute alone, without the "Restriction" attribute defined, functions as a restriction and defines whether a room is bookable or not.

###### Optional attributes:

**Restriction=""**

The restriction attribute may contain "Departure" or "Arrival".

Where a "Close" for Restriction "Arrival" doesn't allow a reservation to be made when guests want to arrive on the selected date. When the restriction is set to "Open" for a certain date, guests are free to make a reservation with arrival on this date whereas a "Close" will restrict guests to book rooms with this arrival date.

A "Close" for Restriction "Departure" doesn't allow a reservation to be made when visitors want to depart on the selected date. When the restriction is set to "Open" for a certain date, guests are free to make a reservation with departure on this date whereas a "Close" will restrict guests to book rooms with this departure date.

Setting restrictions requires passing a RatePlanCode. As RatePlanCode is optional, a message without RatePlanCode does nothing. NB: no errors, no warnings are provided in the response message.

    <AvailStatusMessage>
        <StatusApplicationControl Start="2016-11-05" End="2016-11-17" InvTypeCode="36745603" RatePlanCode="1278606" />
        <RestrictionStatus Restriction="Departure" Status="Open"/> 
    </AvailStatusMessage>


## OTA_HotelAvailNotifRS

All the below code is fixed, except: TimeStamp: 
contains current time Target: set to same value as in corresponding OTA_HotelAvailNotifRQ.

    <?xml version="1.0" encoding="UTF-8"?> 
    <OTA_HotelAvailNotifRS xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05 OTA_HotelAvailNotifRS.xsd" TimeStamp="2016-10-11T15:30:04-00:00" Target="Production" Version="1.004"> 
        <Success /> 
    </OTA_HotelAvailNotifRS>

#### Success

Success element is optional, depending on whether the request contained
mistakes.

### Errors and warnings

#### Warnings

Warnings can be combined with success messages if the request was still
processed.

- **Type**: OTA error code (see OTA codetable). 
- **Code**: OTA EWT code (see OTA codetable).
- **RecordID**: same as LocatorID in OTA_HotelAvailNotifRQ.
- **Status**: NotProcessed (error) / Complete (only warning).
- **ShortText** (may be empty): warning message.





    <?xml version="1.0" encoding="UTF-8"?> 
    <OTA_HotelAvailNotifRS xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05 OTA_HotelAvailNotifRS.xsd" TimeStamp="2016-10-11T15:32:03-00:00" Target="Production" Version="1.004"> 
        <Success /> 
        <Warnings> 
            <Warning Type="1" Code="367" Status="Complete" ShortText="Rooms to Sell were set below the Minimum Contracted rooms for these dates: 2016-11-15, 2016-11-07, 2016-11-12, 2016-11-13, 2016-11-08, 2016-11-09, 2016-11-17, 2016-11-14, 2016-11-06, 2016-11-11, 2016-11-16, 2016-11-05, 2016-11-10. The values have been amended to the Minimum Contracted Rooms."/> 
        </Warnings> 
    </OTA_HotelAvailNotifRS>



#### Errors

Errors are optional and only used alone, without success/warnings.

Code: OTA EWT code (see OTA codetable). 
Type: OTA error code (see OTA codetable).
RecordID: same as LocatorID in OTA_HotelAvailNotifRQ.
Status: NotProcessed.
ShortText (may be empty): error message.

    <?xml version="1.0" encoding="UTF-8"?>
    <OTA_HotelRateAmountNotifRS xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05 OTA_HotelRateAmountNotifRS.xsd" TimeStamp="2016-10-12T13:49:59-00:00" Target="Production" Version="1.006">
        <Errors>
            <Error Type="3" Code="61" RecordID="1" Status="NotProcessed" ShortText="BaseByGuestAmt: rate must be greater than 0."/> 
        </Errors>
    </OTA_HotelRateAmountNotifRS>


## OTA_HotelRateAmountNotif

- Used by partner to send updates for the price per rate/room/date

| Request Type | HTTP POST                                                 |
|:-------------|:----------------------------------------------------------|
| URL          | https://ota-staging.hotelbonanza.com/v1/OTA_HotelRateAmountNotif |
|              | https://ota.hotelbonanza.com/v1/OTA_HotelRateAmountNotif |
| Request      | OTA_HotelRateAmountNotifRQ                               |
| Response     | OTA_HotelRateAmountRS                                    |


    <OTA_HotelRateAmountNotifRQ xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.opentravel.org/OTA/2003/05" xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05 OTA_HotelRateAmountNotifRQ.xsd" TimeStamp="2016-07-12T11:34:05" Target="Production" Version="3.000">
        <RateAmountMessages>
            <RateAmountMessage LocatorID="1">
                <StatusApplicationControl Start="2016-10-19" End="2016-11-20" RatePlanCode="123456" InvTypeCode="987564321"/>
                <Rates>
                    <Rate>
                        <BaseByGuestAmts>
                            <BaseByGuestAmt AmountAfterTax="3800" DecimalPlaces="2" NumberOfGuests="1"/>
                            <BaseByGuestAmt AmountAfterTax="4500" DecimalPlaces="2"/>
                        </BaseByGuestAmts>
                    </Rate>
                </Rates>
            </RateAmountMessage>
            <RateAmountMessage LocatorID="2">
                <StatusApplicationControl Start="2016-10-19" End="2016-11-20" RatePlanCode="123654" InvTypeCode="987654320"/>
                <Rates>
                    <Rate>
                        <BaseByGuestAmts>
                            <BaseByGuestAmt AmountAfterTax="5500" DecimalPlaces="2"/>
                        </BaseByGuestAmts>
                    </Rate>
                </Rates>
            </RateAmountMessage>
        </RateAmountMessages>
    </OTA_HotelRateAmountNotifRQ>

**EXAMPLE RESPONSE:**

    <?xml version="1.0" encoding="UTF-8"?>
    <OTA_HotelRateAmountNotifRS xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05 OTA_HotelRateAmountNotifRS.xsd" TimeStamp="2016-10-12T11:34:06-00:00" Target="Production" Version="1.006">
        <Success />
    </OTA_HotelRateAmountNotifRS>
    

## OTA_HotelRateAmountNotifRQ

All the below XML is fixed and mandatory, except: 
- timestamp: Should contain the current time and date 
- target: Either &#39;Production&#39; or, may contain &#39;Test&#39;, which will not update anything in the HotelBonanza.com database.

.

    <OTA_HotelRateAmountNotifRQ xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.opentravel.org/OTA/2003/05" xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05 OTA_HotelRateAmountNotifRQ.xsd" TimeStamp="2016-10-12T11:34:05" Target="Production" Version="3.000">

#### RateAmountMessages

The RateAmountMessages element is mandatory for the OTA_HotelRateAmountNotif request.

    <RateAmountMessages>
        ...
    </RateAmountMessages>

#### RateAmountMessage

LocatorID (required): should contain a unique ID (used as RecordID in OTA_HotelRateAmountNotifRS)

    <RateAmountMessage LocatorID="1">
    
#### StatusApplicationControl

- Start / End (required): the period which you are updating (inclusive end date). 
*Please note, only dates in the future can be updated.*
- RatePlanCode (required): the HotelBonanza.com rate ID which you are updating.
- InvTypeCode (required): the HotelBonanza.com room ID which you are updating.

&nbsp;

    <StatusApplicationControl Start="2016-10-19" End="2016-11-20" RatePlanCode="1278606" InvTypeCode="36745604"/>

#### Rates

The Rates element is optional for the OTA_HotelRateAmountNotif request.

    <Rates>
    ....
    </Rates>

#### Rate

The Rate element is mandatory for the OTA_HotelRateAmountNotif request when specifying the Rates element.

    <Rate>
    ...
    </Rate>

#### BaseByGuestAmts 

The BaseByGuestAmts element is optional for the OTA_HotelRateAmountNotif request.

    <BaseByGuestAmts>
    ...
    </BaseByGuestAmts>

#### BaseByGuestAmt

The BaseByGuestAmt element is mandatory for the OTA_HotelRateAmountNotif request when specifying the BaseByGuestAmts element.

- AmountAfterTax / AmountBeforeTax (required): the price for the given room for the given date for the given rate category. The currency used for pricing is always the same  for the hotel and set by HotelBonanza.com. The currencies used per country can be found in the documentation under Overview, static information.  Prices cannot be removed after a value has been set. 
*Note: if the room has both &#39;Including VAT&#39; and &#39;Including taxes&#39; enabled in the HotelBonanza.com system, AmountAfterTax must be supplied, otherwise AmountBeforeTax.*
- DecimalPlaces (optional): the number of decimal places for a particular currency (eg. 8550 with DecimalPlaces="2" represents 85.50).
- NumberOfGuests (optional): 1, if set, the single use price is set. 
*Please note, HotelBonanza.com is only able to set prices per room night or for 1 person in a room, per night (so called single-use)*

&nbsp;

    ...
    <BaseByGuestAmt AmountAfterTax="3800" DecimalPlaces="2" NumberOfGuests="1"/>
    <BaseByGuestAmt AmountAfterTax="45.00"/> 
    ...


## OTA_HotelRateAmountNotifRS

All the below code is fixed, except:
TimeStamp: contains current time Target: set to same value as in corresponding OTA_HotelRateAmountNotifRQ.

    <?xml version="1.0" encoding="UTF-8"?>
    <OTA_HotelRateAmountNotifRS xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05 OTA_HotelRateAmountNotifRS.xsd" TimeStamp="2016-10-12T11:47:05-00:00" Target="Production" Version="1.006"> 
        <Success /> 
    </OTA_HotelRateAmountNotifRS>

### Errors and warnings

#### Warnings

Warnings can be combined with success messages if the request was still processed.

Type: OTA error code (see OTA codetable). Code: OTA EWT code (see OTA codetable).
RecordID: same as LocatorID in OTA_HotelAvailNotifRQ.
Status: NotProcessed (error) / Complete (only warning).
ShortText (may be empty): warning message.

    <?xml version="1.0" encoding="UTF-8"?>
    <OTA_HotelRateAmountNotifRS xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05 OTA_HotelRateAmountNotifRS.xsd" TimeStamp="2016-10-12T13:49:09-00:00" Target="Production" Version="1.006"> 
        <Success />
        <Warnings> 
            <Warning Type="1" Code="367" Status="Complete" ShortText="Rate for &#x27;Studio&#x27; (36745604) for &#x27;Standard Rate&#x27; (1278606) for date &#x27; 2016-10-19&#x27; with price &#x27;3800&#x27; might be too low, please check"/> 
        </Warnings>
    </OTA_HotelRateAmountNotifRS>

#### Errors

Errors are optional and only used alone, without success/warnings.

Code: OTA EWT code (see OTA codetable). Type: OTA error code (see OTA
codetable).
RecordID: same as LocatorID in OTA_HotelAvailNotifRQ.
Status: NotProcessed.
ShortText (may be empty): error message.

    <?xml version="1.0" encoding="UTF-8"?> <OTA_HotelRateAmountNotifRS xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05 OTA_HotelRateAmountNotifRS.xsd" TimeStamp="2016-10-12T13:49:59-00:00" Target="Production" Version="1.006"> 
        <Errors>
            <Error Type="3" Code="61" RecordID="1" Status="NotProcessed" ShortText="BaseByGuestAmt: rate must be greater than 0."/> 
        </Errors>
    </OTA_HotelRateAmountNotifRS>


## OTA_HotelResNotif

Used by hotelbonanza.com to send confirmation/Reservation messages to the partner system.


| Request Type | HTTP POST                                         |
|:-------------|:--------------------------------------------------|
| URL          | https://ota-staging.hotelbonanza.com/v1/OTA_HotelResNotif |
| URL          | https://ota.hotelbonanza.com/v1/OTA_HotelResNotif |
| Request      | OTA_HotelResNotifRQ                               |
| Response     | OTA_HotelResNotifRS                               |


    <?xml version="1.0" encoding="UTF-8"?>
    <OTA_HotelResNotifRQ xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05 OTA_HotelResNotifRQ.xsd" TimeStamp="2016-11-13T10:06:51-00:00" Target="Production" Version="1.00">
        <HotelReservations>
            <HotelReservation>
            <RoomStays>
                <RoomStay IndexNumber="123">
                    <RoomTypes>
                        <RoomType RoomTypeCode="1234567">
                            <RoomDescription Name="Standard Double Room - Special conditions - Breakfast included,Lunch included,Dinner included">
                                <Text></Text>
                                <MealPlan>All meals and select beverages are included in the room rate.</MealPlan>
                                <MaxChildren>0</MaxChildren>
                            </RoomDescription>
                            <Amenities>
                                <Amenity></Amenity>
                            </Amenities>
                        </RoomType>
                    </RoomTypes>
                    <RatePlans>
                        <RatePlan>
                            <Commission>
                                <CommissionPayableAmount Amount="4500" DecimalPlaces="2" CurrencyCode="GBP" />
                            </Commission>
                        </RatePlan>
                    </RatePlans>
                    <RoomRates>
                        <RoomRate EffectiveDate="2016-12-13" RatePlanCode="654321">
                            <Rates>
                                <Rate>
                                    <Total AmountAfterTax="37000" DecimalPlaces="2" CurrencyCode="GBP" />
                                </Rate>
                            </Rates>
                        </RoomRate>
                    </RoomRates>
                    <GuestCounts>
                        <GuestCount Count="2" />
                    </GuestCounts>
                    <Total AmountAfterTax="37500" DecimalPlaces="2" CurrencyCode="GBP" />
                    <BasicPropertyInfo HotelCode="123123" />
                    <ResGuestRPHs>
                        <ResGuestRPH RPH="1" />
                    </ResGuestRPHs>
                    <ServiceRPHs>
                        <ServiceRPH RPH="1" />
                    </ServiceRPHs>
                </RoomStay>
            </RoomStays>
            <Services>
                <Service ServiceRPH="1"  ServiceInventoryCode="22"  ServicePricingType="3">
                    <ServiceDetails>
                    <TimeSpan Duration="1" />
                    <Fees>
                        <Fee Amount="5" />
                    </Fees>
                    </ServiceDetails>
                </Service>
            </Services>
            <ResGuests>
                <ResGuest ResGuestRPH="1">
                <Profiles>
                <ProfileInfo>
                    <Profile>
                        <Customer>
                            <PersonName>
                                <Surname>FIRSTNAMEBOOKER LASTNAMEBOOKER</Surname>
                            </PersonName>
                        </Customer>
                    </Profile>
                        </ProfileInfo>
                    </Profiles>
                </ResGuest>
            </ResGuests>
                <ResGlobalInfo>
                    <Comments>
                        <Comment>
                            <Text>SPECIAL REQUESTS</Text>
                        </Comment>
                    </Comments>
                    <Guarantee>
                        <GuaranteesAccepted>
                            <GuaranteeAccepted>
                                <PaymentCard CardCode="VI" CardNumber="4111111111111111" SeriesCode="" ExpireDate="0122">
                                    <CardHolderName>CARD HOLDER NAME</CardHolderName>
                                </PaymentCard>
                            </GuaranteeAccepted>
                        </GuaranteesAccepted>
                    </Guarantee>
                <Total AmountAfterTax="37500" DecimalPlaces="2" CurrencyCode="GBP" />
                <HotelReservationIDs>
                    <HotelReservationID ResID_Value="654321" ResID_Date="2016-11-13T11:06:39" />
                </HotelReservationIDs>
                    <Profiles>
                        <ProfileInfo>
                            <Profile>
                                <Customer>
                                    <PersonName>
                                        <GivenName>John</GivenName>
                                        <Surname>Smith</Surname>
                                    </PersonName>
                                    <Telephone PhoneNumber="+44 1803 123456" />
                                    <Email></Email>
                                    <Address>
                                        <AddressLine>ADDRESS</AddressLine>
                                        <CityName>CITY</CityName>
                                        <PostalCode>ZIP CODE</PostalCode>
                                        <CountryName Code="GB" />
                                        <CompanyName></CompanyName>
                                    </Address>
                                </Customer>
                            </Profile>
                        </ProfileInfo>
                    </Profiles>
                </ResGlobalInfo>
            </HotelReservation>
        </HotelReservations>
    </OTA_HotelResNotifRQ>
    


**EXPECTED EXAMPLE RESPONSE:**

    <OTA_HotelResNotifRS xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05 OTA_HotelResNotifRS.xsd" TimeStamp="2016-10-16T15:39:55" Target="Production" Version="2.001">
        <Success/>
        <HotelReservations>
            <HotelReservation>
                <ResGlobalInfo>
                    <HotelReservationIDs>
                        <HotelReservationID ResID_Value="812864414" ResID_Source=" HotelBonanza.com" ResID_Type="14"/>
                        <HotelReservationID ResID_Value="OWNCODE64143566" ResID_Source="RT" ResID_Type="14"/>
                    </HotelReservationIDs>
                </ResGlobalInfo>
            </HotelReservation>
        </HotelReservations>
    </OTA_HotelResNotifRS>

### Optional parameters in the request URL

<!--#### Element name - hotel_ids

**Short description** The hotel ID supplied by HotelBonanza.com to identify the hotel you are trying to retrieve reservations from.
**Type** integer **Amount of elements possible within parent** min: 1, max: 1 
**Constraints** When &quot;id&quot; is specified, the &quot;hotel_ids&quot; parameter will not have an effect 
<!--**Format** multiple hotel ID&#39;s are separated by commas-->

<!--All hotel IDs are unique.-->

<!--**Syntax**-->

<!--/OTA_HotelResNotif?hotel_ids=10002-->
<!--/OTA_HotelResNotif?hotel_ids=10002,10003

#### Element name - id

**Short description** IT Providers are able to issue a reservation ID to retrieve a single reservation 
**Type** integer 
**Amount of elements possible within parent** Min: 0, Max: 1 
**Constraints** only IDs can be specified that are queued (or have been queued) for XML retrieval before. ID can only be specified if hotel_ids is omitted 
**Format**



**Syntax**

/OTA_HotelResNotif?id=569603872

#### Element name - last_change

**Short description** Retrieves all reservations that have been made or
modified since the specified date in the last_change element **Type**
datetime **Amount of elements possible within parent** Min: 0, Max: 1
**Constraints** you should only request reservations up until 2 weeks in
the past. **Format** YYYY-MM-DD HH:MM:SS

The last change function will not contain any reservation messages that
have never been queued for XML before. IT Providers should only request
reservations up until 2 weeks in the past. The last_change element will
also result in reservations that have been picked up via xml before.

**Syntax**

/OTA_HotelResNotif?last_change=2016-09-01 00:00:00

### Response

At a higher level, OTA_HotelResNotifRQ is a collection of HotelReservations:

    <?xml version="1.0" encoding="UTF-8"?> 
    <OTA_HotelResNotifRQ xmlns="http://www.opentravel.org/OTA/2003/05 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05 OTA_HotelResNotifRQ.xsd" TimeStamp="2009-08-24T15:35:21-00:00" Target="Test" Version="1.003" ResStatus="Commit">
        <HotelReservations> 
            < (hotel reservations, if any) >
        </HotelReservations> 
    </OTA_HotelResNotifRQ>

-->

Each reservation contains:

#### RoomStays

    <RoomStays> 
        <RoomStay IndexNumber="697"> 
            <RoomTypes>
            ...
            </RoomTypes>
        </RoomStay>
    </RoomStays>

RoomStay IndexNumber: the room reservation ID as used by HotelBonanza.com to identify the booked room within the reservation. Unique for every booked room.

#### RoomTypes

    <RoomTypes>
        <RoomType RoomTypeCode="8508109">
            <RoomDescription Name="Test Room 2 - Double Room - Single Use">
                <Text></Text>
                <MealPlan>All meals and select beverages are included in the room rate.</MealPlan>
                <MaxChildren>0</MaxChildren>
            </RoomDescription>
            <AdditionalDetails> 
                <AdditionalDetail>
                    <DetailDescription>
                        <Text>Including service charge</Text>
                    </DetailDescription>
                </AdditionalDetail> 
                <AdditionalDetail>
                    <DetailDescription> 
                        <Text>Excluding breakfast</Text> 
                    </DetailDescription> 
                </AdditionalDetail> 
                <AdditionalDetail>
                    <DetailDescription>
                        <Text>Including VAT</Text>
                    </DetailDescription>
                </AdditionalDetail>
            </AdditionalDetails>
            <Amenities>
                <Amenity>TV</Amenity>
                <Amenity>Telephone</Amenity> 
                <Amenity>Toilet</Amenity>
            </Amenities>
        </RoomType>
    </RoomTypes>

- RoomType RoomTypeCode: the room type ID as used by HotelBonanza.com
- RoomDescription Name: Room short name as displayed on the website. Please note that
  the room name might differ from the room name in the room rates request,
  depending on the policy and/or rate type. Therefore we suggest to only
  map based on room ID and rate ID.
- RoomDescription Text: Room description as currently known for the booked room in the   database.
- RoomDescription MealPlan: Mealplan (breakfast, lunch or dinner) information that is applicable for the booked room.
- RoomDescription MaxChildren: The static setting of maximum amount of children that can stay free in the booked room. Note that, this does not mean that if the max_children=2, that the guest has entered 2 children in the
  bookprocess. This is a static setting, defined per room. The maximum age of the children can be found in the policy of the hotel. The hotelier can request this setting with the HotelBonanza.com account managers or check in the HotelBonanza.com Extranet.
- AdditionalDetails Text: room info as displayed on the website at the time the reservation was made. 
- Amenity: room facility as displayed on the website at the time the reservation was made.

#### RatePlans

    <RatePlans>
        <RatePlan>
            <Commission>
                <CommissionPayableAmount Amount="50" DecimalPlaces="1" CurrencyCode="GBP" />
            </Commission>
        </RatePlan>
    </RatePlans>

- Amount: the total commission due for this room for all nights combined.
- DecimalPlaces: the number of decimal places for a particular currency (eg. 8550 with DecimalPlaces="2" represents 85.50). Please note that HotelBonanza.com supports up to 3 decimals.
- CurrencyCode: the currency used for pricing is always the same for the hotel and set by HotelBonanza.com.

#### RoomRates

Price and rate category ID per night as known at the moment of reservation.

    <RoomRates>
        <RoomRate EffectiveDate="2016-01-28" RatePlanCode="123456">
            <Rates>
                <Rate>
                    <Total AmountAfterTax="5000" DecimalPlaces="2" CurrencyCode="GBP" />
                </Rate>
            </Rates>
        </RoomRate>
    </RoomRates>
       

- EffectiveDate: effective date.
- RatePlanCode: rate category ID.
- AmountAfterTax/AmountBeforeTax: price. (Note: if the room has both &#39;Including VAT&#39; and &#39;Including taxes&#39; enabled in the HotelBonanza.com system,  AmountAfterTax is returned, otherwise AmountBeforeTax)
- DecimalPlaces: the number of decimal places for a particular currency (eg. 8550 with DecimalPlaces="2" represents 85.50). Please note that HotelBonanza.com supports up to 3 decimals.
- CurrencyCode: the currency used for pricing is always the same for the hotel and set by HotelBonanza.com.

#### GuestCounts

    <GuestCounts>
        <GuestCount Count="1" />
    </GuestCounts>

- Count: number of guests for this room as filled in on the website.

#### Total

The total price for this room for all nights combined, sum of all prices known at the moment of reservation.

    <Total AmountAfterTax="6900" DecimalPlaces="2" CurrencyCode="GBP" />

- AmountAfterTax/AmountBeforeTax: price. (Note: if the room has both &#39;Including VAT&#39; and &#39;Including taxes&#39; enabled in the HotelBonanza.com system,  AmountAfterTax is returned, otherwise AmountBeforeTax)
- DecimalPlaces: the number of decimal places for a particular currency (eg. 8550 with DecimalPlaces="2" represents 85.50). Please note that HotelBonanza.com supports up to 3 decimals.
- CurrencyCode: The currency used for pricing is always the same for the hotel and set by HotelBonanza.com.

#### BasicPropertyInfo

    <BasicPropertyInfo HotelCode="12345" />
    
- HotelCode: the hotel ID as used by HotelBonanza.com.

#### ResGuestRPHs

    <ResGuestRPHs>
        <ResGuestRPH RPH="1" />
    </ResGuestRPHs>

- RPH: index for the room guest in this reservation. See also below.

#### SpecialRequests

    <SpecialRequests>
        <SpecialRequest Name="smoking preference">
            <Text>Non-Smoking</Text>
        </SpecialRequest>
    </SpecialRequests>

- Text: Non-Smoking / Smoking.

#### ServiceRPHs


    <ServiceRPHs>
        <ServiceRPH RPH="1" />
        <ServiceRPH RPH="2" />
    </ServiceRPHs>


- RPH: index for the room guest in this reservation. See also below.

#### Services

    <Service ServiceRPH="1" ServiceInventoryCode="1" ServicePricingType="4"> 
        <ServiceDetails> 
            <GuestCounts> 
                <GuestCount Count="1" /> 
            </GuestCounts> 
            <TimeSpan Duration="1" /> 
            <Fees> 
                <Fee Amount="11" /> 
            </Fees> 
        </ServiceDetails>
    </Service>

- ServiceRPH: service identification, used to identify the RoomStay
- ServiceInventoryCode: the add-on id, according to the table:

| **id** | **name**                                                                                                                                        |
|:-------|:------------------------------------------------------------------------------------------------------------------------------------------------|
| 1      | Breakfast                                                                                                                                       |
| 2      | Continental breakfast                                                                                                                           |
| 3      | American breakfast                                                                                                                              |
| 4      | Buffet breakfast                                                                                                                                |
| 5      | Full english breakfast                                                                                                                          |
| 6      | Lunch                                                                                                                                           |
| 7      | Dinner                                                                                                                                          |
| 8      | Half board                                                                                                                                      |
| 9      | Full board                                                                                                                                      |
| 11     | Breakfast for Children                                                                                                                          |
| 12     | Continental breakfast for Children                                                                                                              |
| 13     | American breakfast for Children                                                                                                                 |
| 14     | Buffet breakfast for Children                                                                                                                   |
| 15     | Full english breakfast for Children                                                                                                             |
| 16     | Lunch for Children                                                                                                                              |
| 17     | Dinner for Children                                                                                                                             |
| 18     | Half board for Children                                                                                                                         |
| 19     | Full board for Children                                                                                                                         |
| 20     | WiFi|
| 21     | Internet                                                                                                                                        |
| 22     | Parking space                                                                                                                                   |
| 23     | Extrabed                                                                                                                                        |
| 24     | Babycot                                                                                                                                         |

**ServicePricingType**: 
The pricing type code, according to the table:

| **id** | **code**                                 |
|:-------|:-----------------------------------------|
| 0      | CPM_NOT_APPLICABLE                     |
| 1      | CPM_PER_STAY                           |
| 2      | CPM_PER_PERSON_PER_STAY              |
| 3      | CPM_PER_NIGHT                          |
| 4      | CPM_PER_PERSON_PER_NIGHT             |
| 5      | CPM_PERCENTAGE                          |
| 6      | CPM_PER_PERSON_PER_NIGHT_RESTRICTED |

**ServiceDetails**
The service details, which can include any of these (according to the pricing type code):

- GuestCount (number of guests)
- TimeSpan (number of nights)
- Fees (price to be paid for the add-ons)

#### ResGuests

    <ResGuest ResGuestRPH="1"> 
        <Profiles>
            <ProfileInfo>
                <Profile>
                    <Customer>
                        <PersonName>
                            <Surname>Smith</Surname>
                        </PersonName>
                    </Customer>
                </Profile>
            </ProfileInfo>
        </Profiles>
    </ResGuest>

- ResGuestRPH:  index for this guest in this reservation. See also above.
- Surname (can be empty): guest name for this room as filled in on the  website.

#### ResGlobalInfo, Guarantee

The credit card information. Will only be sent once and deleted
afterwards.

    <Guarantee> 
        <GuaranteesAccepted> 
            <GuaranteeAccepted> 
                <PaymentCard CardCode="VI" CardNumber="4111111111111111" SeriesCode="123" ExpireDate="0120"> 
                    <CardHolderName>Mr John Smith</CardHolderName> 
                </PaymentCard> 
            </GuaranteeAccepted> 
        </GuaranteesAccepted> 
    </Guarantee>

- CardCode: OTA 2 character code of the credit card issuer. If &quot;XX&quot;, something went wrong when retrieving the issuer code (please try again  later):

| **Card Code** | **Description**                    |
|:--------------|:-----------------------------------|
| AX            | American Express                   |
| BC            | Bank Card                          |
| BL            | Carte Bleu                         |
| CB            | Carte Blanche                      |
| DN            | Diners Club                        |
| DS            | Discover Card                      |
| EC            | Eurocard                           |
| JC            | Japanese Credit Bureau Credit Card |
| MA            | Maestro (Switch)                   |
| MC            | Master Card                        |
| TP            | Universal Air Travel Card          |
| VI            | Visa                               |

- CardNumber: credit card number as supplied by the customer. If all 0, something went wrong when retrieving the number (please try again later).
- SeriesCode: credit card CVC-code as supplied by the customer. If all 0, something went wrong when retrieving the CVC-code (please try again later). 
- ExpireDate: credit card expiration date as supplied by the customer. If all 0, something went wrong when retrieving the expiration date (please try   again later).
- CardHolderName: credit card holder&#39;s name as supplied by the customer. If &quot;-&quot;, something went wrong when retrieving the holder&#39;s  name (please try again later).

#### Total

The total amount of room sales of this reservation. All rooms * all nights combined.

    <Total AmountAfterTax="6900" DecimalPlaces="2" CurrencyCode="GBP" />

- AmountAfterTax/AmountBeforeTax: room sales. 
    Note: if all booked rooms have both 'Including VAT' and 'Including taxes' enabled in the HotelBonanza.com system,  AmountAfterTax is returned, otherwise AmountBeforeTax.
- DecimalPlaces: the number of decimal places for a particular currency (eg. 8550 with DecimalPlaces="2" represents 85.50).
- CurrencyCode: The currency used for pricing is always the same for the hotel and set by HotelBonanza.com.

#### HotelReservationIDs

    <HotelReservationIDs>
        <HotelReservationID ResID_Value="223782665" ResID_Date="2010-08-09T11:21:11" />
    </HotelReservationIDs>

- ResID_Value: the reservation is made.

#### Profiles

    <Profiles> 
        <ProfileInfo> 
            <Profile>
                <Customer>
                    <PersonName>
                        <GivenName>John</GivenName>
                        <Surname>Smith</Surname>
                    </PersonName>
                    <Telephone PhoneNumber="01803 123456" />
                    <Email>test@example.com</Email>
                    <Address>
                        <AddressLine>29 High Street</AddressLine>
                        <CityName>Torquay</CityName>
                        <PostalCode>TQ1 1AA</PostalCode>
                        <CountryName Code="GB" /> 
                        <CompanyName></CompanyName>
                    </Address>                    
                </Customer> 
            </Profile> 
        </ProfileInfo>        
    </Profiles>

- GivenName: first name of the booker as supplied by the customer. Note that this
  doesn&#39;t have to be the same as the guestname(s).
- Surname: last name of the booker as supplied by the customer. Note
  that this doesn&#39;t have to be the same as the guestname(s).
- PhoneNumber: telephone number as supplied by the customer.
- Email: email address supplied by the customer. Used by HotelBonanza.com
  to send the reservation confirmation.
- AddressLine: (can be empty): home address supplied by the customer.
- CityName: (can be empty): city of residence as supplied by the customer.
- PostalCode: (can be empty): zip / post code as supplied by the customer.
- CountryName: Code - countrycode of residence as supplied by the customer.
- CompanyName: (can be empty): company name as supplied by the customer.

